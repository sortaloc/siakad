<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">GALERI FASILITAS SEKOLAH</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="5" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="login_psb/foto/IMG-20200829-WA0019.jpg" alt="First slide" style="height: 500px;width: 100%;">
                            <div class="carousel-caption">
                                First Slide
                            </div>
                        </div>
                        <div class="item">
                            <img src="login_psb/foto/IMG-20200829-WA0020.jpg" alt="First slide" style="height: 500px;width: 100%;">
                            <div class="carousel-caption">
                                First Slide
                            </div>
                        </div>
                        <div class="item">
                            <img src="login_psb/foto/IMG-20200829-WA0023.jpg" alt="Second slide" style="height: 500px;width: 100%;">
                            <div class="carousel-caption">
                                Second Slide
                            </div>
                        </div>
                        <div class="item">
                            <img src="login_psb/foto/IMG-20200829-WA0024.jpg" alt="Third slide" style="height: 500px;width: 100%;">
                            <div class="carousel-caption">
                                Third Slide
                            </div>
                        </div>
                        <div class="item">
                            <img src="login_psb/foto/IMG-20200829-WA0025.jpg" alt="Third slide" style="height: 500px;width: 100%;">
                            <div class="carousel-caption">
                                Third Slide
                            </div>
                        </div>
                        <div class="item">
                            <img src="login_psb/foto/IMG-20200829-WA0026.jpg" alt="Third slide" style="height: 500px;width: 100%;">
                            <div class="carousel-caption">
                                Third Slide
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>