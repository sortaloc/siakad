<?php if (@$_GET['act'] == '') { ?>
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Pengumuman
      </div><!-- /.box-header -->
      <div class="box-body">
        <a href="index.php?view=dokumentasi&act=upload" class="btn bg-maroon btn-flat margin">Upload Pengumuman</a></h3>
        <div style='padding:10px;'>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style='width:40px'>No</th>
                <th>Judul</th>
                <th>File</th>
                <th style='width:70px'>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              $detail = mysqli_query($connect, "SELECT * FROM tbl_pengumuman order by id_pengumuan desc");
              foreach ($detail as $data) {
              ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $data['judul']; ?></td>
                  <td><a target="_blank" href="foto_pengumuman/<?php echo $data['file_foto']; ?>"><?php echo $data['file_foto']; ?></a> </td>
                  <td>
                    <center>
                      <a class="btn btn-success btn-xs" title="Edit Data" href="index.php?view=dokumentasi&act=edit&id=<?php echo $data['id_pengumuan']; ?>"><span class="glyphicon glyphicon-edit"></span></a>
                      <a class="btn btn-danger btn-xs" title="Delete Data" href="index.php?view=dokumentasi&act=delete&id=<?php echo $data['id_pengumuan']; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                    </center>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>

        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
<?php
} else if (@$_GET['act'] == 'upload') { ?>
  <div class="col-md-3">
  </div>
  <div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Upload Pengumuman</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method='POST' enctype='multipart/form-data'>
        <div class="box-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Judul</label>
            <input type="text" class="form-control" name="judul">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" name="gambar">
            <p class="help-block">File berbentuk JPG, PNG.</p>
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" name="simpan" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-2">
  </div>
  <?php

  if (isset($_POST['simpan'])) {
    @$judul = $_POST['judul'];
    @$gambar = $_POST['gambar'];

    $dir_gambar = 'foto_pengumuman/';
    $filename = basename($_FILES['gambar']['name']);
    $filenamee = date("YmdHis") . '-' . basename($_FILES['gambar']['name']);
    $uploadfile = $dir_gambar . $filenamee;
    move_uploaded_file($_FILES['gambar']['tmp_name'], $uploadfile);

    $simpan =  mysqli_query($connect, "INSERT INTO tbl_pengumuman VALUES('','$judul','$filenamee')");

    if (!$simpan) {
      echo "<script>alert('DATA BERHASIL DI SIMPAN');window.location='index.php?view=dokumentasi'</script>";
    } else {
      echo "<script>alert('DATA BERHASIL DI SIMPAN');window.location='index.php?view=dokumentasi'</script>";
    }
  }
  ?>
<?php } else if (@$_GET['act'] == 'edit') { ?>
  <?php
  $detail = mysqli_query($connect, "SELECT * FROM tbl_pengumuman where id_pengumuan = '$_GET[id]'");

  foreach ($detail as $data) {
  ?>
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Pengumuman</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method='POST' enctype='multipart/form-data'>
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul</label>
              <input type="text" class="form-control" name="judul_edit" value="<?php echo $data['judul']; ?>">
            </div>
            <div class="form-group">
              <center style="border: dotted;"> <img src="foto_pengumuman/<?php echo $data['file_foto']; ?>" alt="" style="height: 170px;width: 180px;"> </center>
              <br>
              <label for="exampleInputFile">File input</label>
              <input type="text" name="gambar_edit2" value="<?php echo $data['file_foto']; ?>">
              <input type="file" name="gambar_edit">
              <p class="help-block">File berbentuk JPG, PNG.</p>

              <br>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" name="update" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div><!-- /.box -->
    </div>
    <div class="col-md-2">
    </div>
    <?php

    if (isset($_POST['update'])) {
      @$judul_edit = $_POST['judul_edit'];
      @$gambar = $_FILES['gambar_edit']['size'];

      if ($gambar == '' ||  $gambar == 0 || $gambar <= 0) {
        $filenamee = $_POST['gambar_edit2'];
      } else {
        $dir_gambar = 'foto_pengumuman/';
        $filename = basename($_FILES['gambar_edit']['name']);
        $filenamee = date("YmdHis") . '-' . basename($_FILES['gambar_edit']['name']);
        $uploadfile = $dir_gambar . $filenamee;
        move_uploaded_file($_FILES['gambar_edit']['tmp_name'], $uploadfile);
      }
      $simpan = mysqli_query($connect, "UPDATE tbl_pengumuman SET judul='$judul_edit', file_foto='$filenamee' where id_pengumuan='$_GET[id]'");

      if (!$simpan) {
        echo "<script>alert('DATA GAGAL DI SIMPAN');window.location='index.php?view=dokumentasi'</script>";
      } else {
        echo "<script>alert('DATA BERHASIL DI SIMPAN');window.location='index.php?view=dokumentasi'</script>";
      }
    }
    ?>
  <?php } ?>
<?php } else if (@$_GET['act'] == 'delete') { ?>
  <?php
  $sql = "DELETE From tbl_pengumuman Where id_pengumuan='$_GET[id]'";
  $result = mysqli_query($connect, $sql);

  if (!$result) {
    echo "<script>alert('DATA GAGAL DI HAPUS');window.location='index.php?view=dokumentasi'</script>";
  } else {
    echo "<script>alert('DATA TELAH DI HAPUS');window.location='index.php?view=dokumentasi'</script>";
  }

  ?>
<?php } ?>